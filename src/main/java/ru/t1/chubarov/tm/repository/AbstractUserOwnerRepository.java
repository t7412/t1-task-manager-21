package ru.t1.chubarov.tm.repository;

import ru.t1.chubarov.tm.api.repository.IUserOwnerRepository;
import ru.t1.chubarov.tm.model.AbstractUserOwnerModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnerRepository<M extends AbstractUserOwnerModel> extends AbstractRepository<M> implements IUserOwnerRepository<M> {

    @Override
    public void clear(final String userId) {
        final List<M> model = findAll(userId);
        clear();
    }

    @Override
    public List<M> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return records
                .stream()
                .filter(m -> userId.equals(m.getUserid()))
                .collect(Collectors.toList());
    }

    @Override
    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Override
    public M findOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty() || id == null) return null;
        return records
                .stream()
                .filter(m -> userId.equals(m.getUserid()))
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        return records
                .stream()
                .filter(m -> userId.equals(m.getUserid()))
                .skip(index)
                .findFirst()
                .orElse(null);
    }

    @Override
    public int getSize(final String userId) {
        return (int) records
                .stream()
                .filter(m -> userId.equals(m.getUserid()))
                .count();
    }

    @Override
    public M remove(final String userId, final M model) {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

    @Override
    public M removeById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        final M model = findOneById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public M removeByIndex(final String userId, final Integer index) {
        final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public M add(final String userId, M model) {
        if (userId == null) return null;
        model.setUserid(userId);
        return add(model);
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        return findOneById(userId, id) != null;
    }


}
