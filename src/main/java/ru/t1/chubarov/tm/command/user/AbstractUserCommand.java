package ru.t1.chubarov.tm.command.user;

import ru.t1.chubarov.tm.api.service.IUserService;
import ru.t1.chubarov.tm.command.AbstractCommand;
import ru.t1.chubarov.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    public String getArgument() {
        return null;
    }

    public void showUser(final User user) {
        if (user == null) return;
        System.out.println("ID: " + user.getId());
        System.out.println("  LOGIN: " + user.getLogin());
        System.out.println("  EMAIL: " + user.getEmail());
        System.out.println("  FIRST NAME: " + user.getFirstName());
        System.out.println("  LAST NAME: " + user.getLastName());
        System.out.println("  MIDDLE NAME: " + user.getMiddleName());
    }

}
