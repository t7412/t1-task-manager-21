package ru.t1.chubarov.tm.command.task;

import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.util.TerminalUtil;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[START TASK BY ID");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        getTaskService().changeTaskStatusById(userId, id, Status.IN_PROGRESS);
    }

    @Override
    public String getName() {
        return "task-start-by-id";
    }

    @Override
    public String getDescription() {
        return "Update task status to [In progress] by id.";
    }

}
