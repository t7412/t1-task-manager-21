package ru.t1.chubarov.tm.command.task;

import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.model.Task;
import ru.t1.chubarov.tm.util.TerminalUtil;

import java.util.List;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("[ENTER ID:]");
        final String userId = getUserId();
        final String id = TerminalUtil.nextLine();
        final Task task = getTaskService().findOneById(userId,id);
        showTask(userId, task);
    }

    @Override
    public String getName() {
        return "task-show-by-id";
    }

    @Override
    public String getDescription() {
        return "Display task by id.";
    }

}
