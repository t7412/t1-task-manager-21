package ru.t1.chubarov.tm.command.project;

import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.model.Project;
import ru.t1.chubarov.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {
    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = getUserId();
        final Project project = getProjectService().findOneByIndex(userId, index);
        showProject(project);
    }

    @Override
    public String getName() {
        return "project-show-by-index";
    }

    @Override
    public String getDescription() {
        return "Display project by index.";
    }

}
