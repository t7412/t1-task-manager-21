package ru.t1.chubarov.tm.api.repository;

import ru.t1.chubarov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnerRepository<Task> {

    List<Task> findAllByProjectId(String userId, String projectId);
}
