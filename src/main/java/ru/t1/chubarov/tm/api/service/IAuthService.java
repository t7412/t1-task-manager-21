package ru.t1.chubarov.tm.api.service;

import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.exception.user.AccessDeniedException;
import ru.t1.chubarov.tm.model.User;

public interface IAuthService {

    void login(String login, String password) throws AbstractException;

    void logout();

    boolean isAuth();

    String getUserId() throws AccessDeniedException;

    User getUser() throws AbstractException;

    void checkRoles(Role[] roles) throws AbstractException;

}
