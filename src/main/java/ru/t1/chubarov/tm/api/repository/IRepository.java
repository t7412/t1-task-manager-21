package ru.t1.chubarov.tm.api.repository;

import ru.t1.chubarov.tm.enumerated.Sort;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.model.AbstractModel;
import ru.t1.chubarov.tm.model.AbstractUserOwnerModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    M add(M model) throws AbstractException;

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    List<M> findAll(Sort sort);

    M findOneById(String id) throws AbstractException;

    M findOneByIndex(Integer index) throws AbstractException;

    M remove(M model) throws AbstractException;

    M removeById(String id) throws AbstractException;

    void clear();

    int getSize();

    boolean existsById(String id);

}
